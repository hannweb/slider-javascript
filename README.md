this project was created by www.hannwebsites.com

## Overview

This project was created to support slider in elementor free version. This project will inherit css from the global template except for those that were required. For instance font color for header as default were blue.

this template is regularly used for banners which has a background photo, cover to create opacity, and 2 columns to place text, image or both.

## how to use it.

1. clone the project
2. update HTML with your text and or add more sections (Javascripts recongnize the section to loop thru them)
3. update the CSS with your corresponding background images. and if you add more sections set up the background fo them.
4. copy and paste in word-press.

## Questions?

contact info@hannwebsites.com

## contributing

fork the project and push changes thru it,I will review and merge.

const carouselSlide = document.querySelector('.home-banner-slides');
const carouselContent = document.querySelectorAll('.home-banner-slides section')
const nextButton = document.querySelector('#next-button')
const prevButton = document.querySelector('#prev-button')

let counter = 1
const size = carouselContent[0].clientWidth;
const carouselLength = carouselContent.length;

// functions

setInterval(() => {
    if (counter === 0) {
        counter++
        return
    } if (counter < carouselLength) {
        carouselSlide.style.transition = 'transform 0.4s ease-in-out';
        carouselSlide.style.transform = 'translateX(' + (-size * counter) + 'px)';
        counter++
    } else {
        carouselSlide.style.transition = 'none';
        counter = 0
        carouselSlide.style.transform = 'translateX(' + (-size * counter) + 'px)';
        counter++
    }
}, 5000);

console.log(counter)
nextButton.addEventListener('click', () => {
    if (counter === 0) {
        counter++
        carouselSlide.style.transition = 'transform 0.4s ease-in-out';
        carouselSlide.style.transform = 'translateX(' + (-size * counter) + 'px)';
        console.log("counter 0")
        return
    } else if (counter < carouselLength) {
        carouselSlide.style.transition = 'transform 0.4s ease-in-out';
        carouselSlide.style.transform = 'translateX(' + (-size * counter) + 'px)';
        counter++
        console.log("regular", counter)
    } else {
        carouselSlide.style.transition = 'none';
        counter = 0
        carouselSlide.style.transform = 'translateX(' + (-size * counter) + 'px)';
        counter++
        console.log("reset", counter)
    }
});

prevButton.addEventListener('click', () => {
    if (counter === 1) {
        counter--
        carouselSlide.style.transition = 'transform 0.4s ease-in-out';
        carouselSlide.style.transform = 'translateX(' + (-size * counter) + 'px)';
        console.log("regular", counter)
    } else if (counter <= 0) {
        carouselSlide.style.transition = 'none';
        counter = carouselLength - 1
        carouselSlide.style.transform = 'translateX(' + (-size * counter) + 'px)';
        console.log("counter 0", counter)
        return
    } else {
        counter--
        carouselSlide.style.transition = 'transform 0.4s ease-in-out';
        carouselSlide.style.transform = 'translateX(' + (-size * counter) + 'px)';
    }

});